package com.signaleprobleme.api;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "region", path = "region")
public interface RegionRepository extends MongoRepository<Region,String> {
    List<Region> findByNomRegion(@Param("nomRegion") String nomRegion);
}
