package com.signaleprobleme.api;


import org.springframework.data.annotation.Id;

public class Region {
    @Id private String Id;
    private String nomRegion;

    public String getNomRegion() {
        return nomRegion;
    }
}
