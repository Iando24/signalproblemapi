package com.signaleprobleme.api;


import org.springframework.data.annotation.Id;

public class TypeSignal {
    @Id private String Id;
    private String typeSignal;

    public String getTypeSignal() {
        return typeSignal;
    }

    public void setTypeSignal(String typeSignal) {
        this.typeSignal = typeSignal;
    }
}
