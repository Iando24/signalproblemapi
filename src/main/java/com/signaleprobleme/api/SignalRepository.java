package com.signaleprobleme.api;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Date;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "signal", path = "signal")
public interface SignalRepository extends MongoRepository<Signal, String> {
    List<Signal> findByTerritoire(@Param("territoire") String territoire);
    List<Signal> findByStatus(@Param("status") String status);
    List<Signal> findByType(@Param("type") String type);
    List<Signal> findByNamePerson(@Param("NamePerson") String NamePerson);
    List<Signal> findByNomRegion(@Param("NomRegion") String NomRegion);
    List<Signal> findByDateSignal(@Param("dateSignal") Date dateSignal);
}
