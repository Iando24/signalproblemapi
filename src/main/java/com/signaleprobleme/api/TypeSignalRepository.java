package com.signaleprobleme.api;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Date;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "typeSignal", path = "typeSignal")
public interface TypeSignalRepository extends MongoRepository<TypeSignal,Date> {
    List<TypeSignal> findByTypeSignal(@Param("typeSignal") Date typeSignal);
}
